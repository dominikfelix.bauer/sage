#!/usr/local/sage-6.10/local/bin/python2.7

import plotly
#plotly.__version__
from plotly.offline import download_plotlyjs, init_notebook_mode, iplot

# init offline mode
init_notebook_mode() 

# get all objects
import plotly.graph_objs as go

# tables
from plotly import figure_factory as ff

# numpy as workhorse
import numpy as np
    
# magic function from sage to make evaluation of symbolic functions correct 
from sage.plot.misc import setup_for_eval_on_grid

# TODO: Lichteffekte bei gleicher Farbe noch ueberarbeiten
def plot3d_ly (fun, pr1, pr2, plot_points= [30, 30], opacity=1, color=None, zauto=False, showscale=True, **kwargs):
    """ 
    Creates plotly 3d-plot.

    INPUT:
    
    - ``fun``: function (expression) to plot
    - ``pr1``: range for first parameter in tuple (parameter, a, b)
    - ``pr2``: range for second parameter in tuple (parameter, a, b)
    - ``plot_points`` : number of points in every axis given as list.    

    OUTPUT: 
    
    - plotly surface object
    """
    realfun, ranges = setup_for_eval_on_grid(fun, [pr1,pr2], 30)

    x = np.linspace(ranges[0][0], ranges[0][1], plot_points[0])
    y = np.linspace(ranges[1][0], ranges[1][1], plot_points[1])
    xGrid, yGrid = np.meshgrid(x, y)

    
    z = np.array(map(lambda x: realfun(x[0],x[1]), zip(xGrid.flatten(),yGrid.flatten())) ).reshape(plot_points)
    
    if color == None:
        surface = go.Surface(x=xGrid, y=yGrid, z=z, opacity=opacity, showscale=showscale, **kwargs)
    else:
        surface = go.Surface(x=xGrid, y=yGrid, z=z, opacity=opacity, zauto=zauto, colorscale=[[0, color], [1, color]], showscale=showscale, **kwargs)
    return surface

def plotdens_ly (fun, pr1, pr2, plot_points= [50, 50], **kwargs):
    """ 
    Creates plotly density plot.

    INPUT:
    """
    realfun, ranges = setup_for_eval_on_grid(fun, [pr1,pr2], 30)
    
    x = np.linspace(ranges[0][0], ranges[0][1], plot_points[0])
    y = np.linspace(ranges[1][0], ranges[1][1], plot_points[1])
    xGrid, yGrid = np.meshgrid(x, y)

    z = np.array(map(lambda x: realfun(x[0],x[1]), zip(xGrid.flatten(),yGrid.flatten())) ).reshape(plot_points)
    data = go.Heatmap(zsmooth='best', z=z, **kwargs)
    
    return data

def plotcontour_ly (fun, pr1, pr2, plot_points= [100, 100], contours=dict(
            start=0,
            end=8,
            size=2,
        ), **kwargs):
    """ make plotly contour-plot.

    INPUT:
    
    - ``fun``: function (expression) to plot
    - ``pr1``: range for first parameter in tuple (parameter, a, b)
    - ``pr2``: range for second parameter in tuple (parameter, a, b)
    - ``plot_points`` : number of points in every axis given as list.    

    OUTPUT: 
    
    - plotly contour object
    """
    realfun, ranges = setup_for_eval_on_grid(fun, [pr1,pr2], 30)

    x = np.linspace(ranges[0][0], ranges[0][1], plot_points[0])
    y = np.linspace(ranges[1][0], ranges[1][1], plot_points[1])
    xGrid, yGrid = np.meshgrid(x, y)

    z = np.array(map(lambda x: realfun(x[0],x[1]), zip(xGrid.flatten(),yGrid.flatten())) ).reshape(plot_points)
    data = go.Contour(z=z,contours=contours, **kwargs)

    return data

def heatmap_ly (matrix, smooth='best', **kwargs):
    """
    Creates heatmap of given matrix.
    """
    data = go.Heatmap(zsmooth=smooth, z=matrix.numpy().tolist(), **kwargs )
    return data


def sphere_ly(r=1, precision=40, **kwargs):
    """
    Creates plotly sphere object.
    """
    a = np.linspace(0, np.pi, np.ceil(precision/2))
    b = np.linspace(0, 2*np.pi, precision)
    c, d = np.meshgrid(a,b)
    z1 = r*np.sin(c)*np.cos(d)
    z2 = r*np.sin(c)*np.sin(d)
    z3 = r*np.cos(c)
    surface = go.Surface(x=z1, y=z2, z=z3, showscale=False, **kwargs)
    return surface

def point3d_ly(pointlist, size = 5, **kwargs):
    """
    Create 3D-point with plotly.
    """
    xcoord = map(lambda x: x[0], pointlist)
    ycoord = map(lambda x: x[1], pointlist)
    zcoord = map(lambda x: x[2], pointlist)
    point = go.Scatter3d(x=xcoord, y=ycoord, z=zcoord, mode='markers',
    marker=dict(
        size=size,
        line=dict(
            color='rgba(217, 217, 217, 0.14)',
            width=0.5
        ),
        opacity=0.9
      ), **kwargs 
    )
    return point

def parametricplot_ly (fun, pr1, pr2, points= 30, **kwargs):
    """
    Create plotly parametric plot 2d (surfaces)
    """
    realfun, ranges = setup_for_eval_on_grid(fun, [pr1,pr2], 30)
    
    lc = np.linspace(ranges[0][0], ranges[0][1], points)
    mc = np.linspace(ranges[1][0], ranges[1][1], points)
    lGrid, mGrid = np.meshgrid(lc, mc)

    x = np.array(map(lambda x: realfun[0](x[0],x[1]), zip(lGrid.flatten(),mGrid.flatten())) ).reshape((points,points))
    y = np.array(map(lambda x: realfun[1](x[0],x[1]), zip(lGrid.flatten(),mGrid.flatten())) ).reshape((points,points))
    z = np.array(map(lambda x: realfun[2](x[0],x[1]), zip(lGrid.flatten(),mGrid.flatten())) ).reshape((points,points))
    
    surface = go.Surface(x=x, y=y, z=z, **kwargs)
    return surface

#possible network-nodes /graphs
def graph_ly(G):
  """
  Makes a network-Graph with plotly from given sage-Graph-object.
  """
  lay = G.layout()
  edge_trace = go.Scatter(
    x=[], 
    y=[], 
    line=go.Line(width=0.5,color='#888'),
    hoverinfo='none',
    mode='lines')

  for edge in G.edges():
    x0, y0 = lay[edge[0]]
    x1, y1 = lay[edge[1]]
    edge_trace['x'] += [x0, x1, None]
    edge_trace['y'] += [y0, y1, None]
    
  for node in G.vertices():
    length = list()
    length.append(len(str(node)))

  if max(length) <= 9:
    lsize = max(length)
    ltaken = "markers+text"
  else:
    lsize = 3
    ltaken = "markers"

  marker2 = go.Marker(color='#6175c1', symbol='circle', size=10*lsize, opacity=0.9, line=dict(width=2))

  node_trace = go.Scatter(
    x=[], 
    y=[], 
    text=[],
    mode=ltaken, 
    hoverinfo='text',
    name='',
    marker=marker2)

  for node in G.vertices():
    x, y = lay[node]
    node_trace['x'].append(x)
    node_trace['y'].append(y)
    node_trace['text'].append(str(node))    

  axis = dict(showline=False, # hide axis line, grid, ticklabels and  title
            zeroline=False,
            showgrid=False,
            showticklabels=False,
            )

  layout = dict(title= 'Tree',
              font=dict(size=12),
              showlegend=False,
              xaxis=go.XAxis(axis),
              yaxis=go.YAxis(axis),          
              margin=dict(l=40, r=40, b=85, t=100),
              hovermode='closest',
              plot_bgcolor='rgb(248,248,248)'          
              )
  ishow3d([edge_trace, node_trace],layout=layout)

def create_streamline_ly(fun, pr1, pr2, points=100, **kwargs):
  """
  Creates streamline with plotly
  """
  realfun, ranges = setup_for_eval_on_grid(fun, [pr1,pr2], 30)
  lc = np.linspace(ranges[0][0], ranges[0][1], points)
  mc = np.linspace(ranges[1][0], ranges[1][1], points)
  lGrid, mGrid = np.meshgrid(lc, mc)
  u = np.array(map(lambda x: realfun[0](x[0],x[1]), zip(lGrid.flatten(),mGrid.flatten())) ).reshape((points,points))
  v = np.array(map(lambda x: realfun[1](x[0],x[1]), zip(lGrid.flatten(),mGrid.flatten())) ).reshape((points,points))
  fig = ff.create_streamline(lc, mc, u, v, arrow_scale=.1, **kwargs)
  return fig


# TODO: implicit not working properly
def implicit_ly (fun, pr1, pr2, pr3, points= 30):
    realfun, ranges = setup_for_eval_on_grid(fun, [pr1,pr2,pr3], points)
    print (realfun)
    print (ranges)

def polygon3d_ly(x, y, z, **kwargs):
  """
  Creates a Polygon from given set of coordinates.
  """
  a=go.Mesh3d(x=x, y=y, z=z, alphahull=-1, color='90EE90', **kwargs)
  return a
    
def parametricplotline_ly (fun, pr, points= 20, **kwargs):
    """
    Create plotly parametric plot 1d (lines)

    INPUT:

    - ``f`` - function
    - ``pr`` - range of the parameter
    - ``points`` - (default: 20) the number of function evaluations in each direction.

    OUTPUT:

    EXAMPLES::
    """
    realfun, ranges = setup_for_eval_on_grid(fun, [pr], points)

    kc = np.linspace(ranges[0][0],ranges[0][1],points)
    x = np.array(map(lambda x: realfun[0](x), kc))
    y = np.array(map(lambda x: realfun[1](x), kc))
    z = np.array(map(lambda x: realfun[2](x), kc))
    
    line = go.Scatter3d(x=x, y=y, z=z, mode='lines', line=dict( color='rgb(0,0,0)', width=4), **kwargs )
    
    return line



def ishow3d(data, layout= go.Layout(
    title='Plot',
        scene=dict(
            xaxis=dict(
                gridcolor='rgb(255, 255, 255)',
                zerolinecolor='rgb(255, 255, 255)',
                showbackground=True,
                backgroundcolor='rgb(230, 230,230)'
            ),
            yaxis=dict(
                gridcolor='rgb(255, 255, 255)',
                zerolinecolor='rgb(255, 255, 255)',
                showbackground=True,
                backgroundcolor='rgb(230, 230,230)'
            ),
            zaxis=dict(
                gridcolor='rgb(255, 255, 255)',
                zerolinecolor='rgb(255, 255, 255)',
                showbackground=True,
                backgroundcolor='rgb(230, 230,230)'
            )
          )
      ), **kwargs
    ):
    """
    displays given data with the layout in a plotly javascript-frame.
    """
    for i,val in kwargs.iteritems():
      layout[i]=val
    fig = go.Figure(data=data, layout=layout)    
    iplot(fig)

