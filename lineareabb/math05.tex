\documentclass[a4paper,12pt,DIV15]{scrartcl}
\usepackage[xetex,bookmarks=true,pdfstartview=FitH,bookmarksopen=true,
    colorlinks,citecolor=Blue,linkcolor=DarkBlue,urlcolor=Green,
    pagebackref=true,plainpages=false,pdfpagelabels=true,unicode=true,
    breaklinks=true,naturalnames=false,setpagesize=true,a4paper=true,hyperindex]{hyperref}
\usepackage[svgnames,hyperref]{xcolor} %color definition
\usepackage{tikz}
\input{common_header}
\begin{document}
\begin{center}
    \textbf{\LARGE Einführung in Sage}\\
    {\large Einheit 05}\\
    {\large Zusammenfassung: lineare Abbildungen}
\end{center}



\begin{defn}[Koordinatenvektor]
Sei $V$ ein $K$-Vektorraum mit Basis $\mathcal{V}=(v_1, \dots
,v_n)$.
\end{defn}

\begin{defn}[Isomorphismus]
Seien $K$-Vektorräume $V$ und $W$ mit Basen $\mathcal{V}=(v_1, \dots
,v_n)$ und $\mathcal{W}=(w_1, \dots ,w_m)$ gegeben. 

 Für eine Matrix $A\in K^{m
 \times n}$ wird durch
\begin{eqnarray*}
F(v_1) & := & a_{11}w_1 + \dots +a_{m1} w_m\\
\vdots &    & \vdots     \\
F(v_n) & := & a_{1n} w_1 + \dots + a_{mn}w_m
\end{eqnarray*}
eine lineare Abbildung $F$ definiert. Dies ergibt einen Isomorphismus
\[
L^\mathcal{V}_\mathcal{W}: K^{m \times n} \ \rightarrow \
\mathrm{Hom}_K(V,W), \quad A \ \mapsto \ F.
\] 
\end{defn}

\paragraph{Kanonisches Beispiel:}
Seien $K^n$ und $K^m$ mit den kanonischen Basen $\mathcal{K}_n$ und
$\mathcal{K}_m$ versehen.
\begin{itemize}
\item Die Abbildungen $\Phi_{\mathcal{K}_n}$ und $\Phi_{\mathcal{K}_m}$
sind Identitäten.
\item  Die Abbildung $L^{\mathcal{K}_n}_{\mathcal{K}_m}$ ist gegeben
durch 
\[ L^{\mathcal{K}_n}_{\mathcal{K}_m} (A)(x)=Ax,\; x \in K^n. \]
\item Die Spaltenvektoren von $A$ sind die Bilder der Einheitsvektoren
unter der Abbildung  $L^{\mathcal{K}_n}_{\mathcal{K}_m}(A)$.
\end{itemize}

\begin{defn}[Kommutierendes Diagramm]
  Seien $K$-Vektorräume $V$ und $W$ mit Basen $\mathcal{V}=(v_1, \dots
,v_n)$ und $\mathcal{W}=(w_1, \dots ,w_m)$ und eine lineare Abbildung
$F$ gegeben. Dann gilt das folgende kommutierende Diagramm:\\
\begin{center}
  \begin{tikzpicture}
  \draw (0,2) node[] (a) {$V$}
   (5,2) node[] (b) {$W$}
   (0,0)  node[] (c) {$K^n$}
   (5,0)  node[] (d) {$K^m$};
  \draw[-latex,thick] (a) -- (b) node[midway, above] {$F$};
 \draw[-latex,thick] (d) -- (b) node[midway,right] {$\Phi_W$};
  \draw[-latex,thick] (c) -- (d) node[midway,below] {$(L_\mathcal{W}^\mathcal{V})^{-1}(F)$};
  \draw[-latex,thick] (c) -- (a) node[midway,left] {$\Phi_V$};
  \end{tikzpicture}
\end{center}
\end{defn}

\section{Eigenwerte und Eigenvektoren}
\begin{defn}[Eigenwert und Eigenvektor]
Sei $A\in K^{n\times n}$. Ein Element $\lambda \in K$ heißt {\color{red} Eigenwert} von
$A$, wenn ein $x \in K^n\smallsetminus \{0 \}$ existiert, 
\[ {\color{red}A x = \lambda x} \] 
gilt. Der Vektor $x \in K^n$ heißt {\color{red} Eigenvektor} zum Eigenwert $\lambda$.
\begin{itemize}
\item Die Eigenwerte sind die Nullstellen des {\color{red} charakteristischen
Polynoms} 
\[p(t):=\det(A-t \ I_n).\] 
\item Es gibt höchstens $n$ Eigenwerte.
\end{itemize}
\end{defn}

\paragraph{Bemerkungen}
\begin{itemize}
\item Eigenvektoren zu paarweise verschiedenen Eigenwerten  sind linear
unabhängig.
\item Gibt es eine Basis aus Eigenvektoren, so ist $A$
{\color{red} diagonalisierbar}, d.h. man kann die Abbildung $L_A$ bei
geeigneter Basiswahl durch eine Diagonalmatrix repräsentieren.
\item Jeder Endomorphismus eines komplexen Vektorraums läßt sich durch
eine Matrix in \textsl{Jordanscher Normalform} darstellen.
\end{itemize}


\begin{defn}[Lineare Gleichungssysteme (LGS)]
Sei $A\in K^{m \times n}$ und $b \in K^m$. Gesucht ist die Menge der
Lösungen (Lösungsraum): 
\[ \{ x \in K^n \;|\; A x = b\} \]
\begin{itemize}
\item Ist $b=0$, so spricht man von einem {\color{red} homogenen
System}. Ansonsten spricht man von einem {\color{red} inhomogenen System}.
\item Der Lösungsraum $W$ des homogenen Systems bildet einen
Untervektorraum des $K^n$. Die Dimension ist 
\[ \dim (W)=n - \rang(A). \] 
\end{itemize}
\end{defn}

\paragraph{Struktur des Lösungsraums:}
\begin{itemize}
\item {\color{red} affiner Unterraum}  $X \subset K^n$: wenn ein
Unterraum $W$ von $K^n$ und ein $v \in K^n$ existiert, so dass 
\[X=v+W\]
%Der Unterraum $W$ ist durch $X$ eindeutig bestimmt, $v$ kann jeder Vektor aus $X$ sein.
\item Die Lösungen des inhomogenen Systems ($b \neq 0$) bilden einen affinen
Unterraum des $K^n$.   

\item Ist $W$ der Lösungsraum des homogenen Systems und $v \in K^n$
eine beliebige Lösung von $Ax=b$, dann ist der Lösungsraum $X$ von $Ax=b$
gegeben durch  $X=v+W$.
\item Zwei Lösungen des inhomogenen Systems unterscheiden sich durch
eine Lösung des homogenen Systems.
\end{itemize}

\begin{thm}[Lösbarkeit]
\begin{itemize}
\item Das inhomogene System ist genau dann für alle $b$
lösbar, wenn $\rang(A)=m$ gilt.
\item Das homogene bzw. das inhomogene System besitzt höchstens eine
Lösung, genau dann wenn $\rang(A)=n$ gilt.
\item Der Lösungsraum des inhomogenen Systems ist genau dann nicht
leer, wenn $\rang(A)=\rang(A, b)$ gilt. %A,b : erweiterte Koeffizientenmatrix
\item Praktisch kann ein LGS mit dem {\color{red} Gausschen
Eliminationsverfahren} gelöst werden.
\end{itemize}
\end{thm}
\end{document}
