#!/usr/bin/python3 

from plumbum import local
import argparse

p = argparse.ArgumentParser()
p.add_argument('--type', help='type of link generator', default="lecture")
args = p.parse_args()

# change to maindir 
fp = local.path(__file__)
local.cwd.chdir(fp.dirname / "..")

print (local.cwd)

readme = local.cwd / 'README.md'
tmp = local.cwd / 'README.md.tmp'


if args.type == "lecture":
  header = "## Vorlesungs-notebooks"
  linkbase = "gitlab.gwdg.de/jschulz1/sage/raw/master/"
  directory = "lecture"
elif args.type == "exercises":        
  header = "## Übungsaufgaben"
  linkbase = "gitlab.gwdg.de/jschulz1/sage/raw/master/"
  directory = "exercises"

# jump to correct place and save stuff up to start of list
with readme.open('r') as f, tmp.open('w') as fout:
    flag = True
    for line in f:
        if flag:
            fout.write(line)

        if not flag and line.find("## ") != -1:
            flag = True
            fout.write(line)

        if flag and line.find(header) != -1:
            flag = False
            fout.write("\n")
            for nb in sorted(local.path(directory) // "??_*.ipynb"):
                title = nb.basename.replace(".ipynb", "")
                link = "* [{}](http://nbviewer.jupyter.org/urls/{}{}/{}.ipynb)\n".format(title, linkbase, directory, title)
                print(link)
                fout.write(link)
            fout.write("\n")

tmp.rename(readme)
